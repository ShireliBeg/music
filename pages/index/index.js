//index.js
//获取应用实例
import { ClassicModel } from '../../models/classic.js'
import { LikeModel } from '../../models/like.js'
import { CommonModel } from '../../models/login.js'
let classModel = new ClassicModel()
let likeModel = new LikeModel()
let testModel = new CommonModel()

Page({
  data: {
    classic: {},
    first: false, //第一期就禁用右侧的按钮
    latest: true //最后一期禁用左侧的按钮
  },
  onLoad: function (options) {
    this.testToken()
     this.getInitData()
  },
  getInitData() {
    classModel.getFavLastest(res => {
      this.setData({
        classic: res
      })
    })
  },
  testToken() {
    testModel.demoModel().then(res=>{
      console.log(res)
    }).catch(err=>{})
  },
  handClick(event) {
    let isClick = event.detail.typeClick
    likeModel.getLike(isClick, this.data.classic.id, this.data.classic.type)
  },
  onLeft(event) {
    this._updateClassic('next')
  },
  onRight(event) {
    this._updateClassic('previous')
  },
  _updateClassic(nextOrPrevious) {
    let index = this.data.classic.index
    classModel.handleChangePage(index, nextOrPrevious, (res) => {
      console.log(res)
      this.setData({
        classic: res,
        first: classModel._isFirst(res.index),
        latest: classModel._isLatest(res.index)
      })
    })
  }
})
