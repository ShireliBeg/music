// components/navi/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    latest: {
      type: Boolean,
      value: false
    },
    first: {
      type: Boolean,
      value: false
    },
    title: {
      type: String,
      value: '.....'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    disLeftSrc: "../images/triangle.dis@left.png",
    highLeftSrc: "../images/triangle@left.png",
    disRightSrc: "../images/triangle.dis@right.png",
    highRightSrc: "../images/triangle@right.png",
  },

  /**
   * 组件的方法列表
   */
  methods: {
    handleLeft() {
      if (!this.properties.latest) {
        this.triggerEvent('latest', {}, {})
      }
    },
    handleRight() {
      if (!this.properties.first) {
        this.triggerEvent('first', {}, {})
      }
    }
  }
})
