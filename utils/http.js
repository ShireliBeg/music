let myApp = getApp()
const tip = {
  1: '抱歉，出现错误',
  1005: 'appkey无效, 请联系管理员！',
  3000: '数据不存在'
}
class HTTP {
  request(params) {
    let url = myApp.globalData.baseUrl + params.url
    wx.request({
      url: url,
      data: params.data,
      header: {
        'content-type': 'application/json', // 默认值
        appkey: myApp.globalData.appkey
      },
      method: params.method || 'GET',
      success: res => {
        let code = res.statusCode.toString()
        if(code.startsWith('2')){
          params.success && params.success(res.data)
        }else{
          let errorCode = res.data.error_code || 1
          this._errorMsg(errorCode)
        }
      },
      fail: err => {
        this._errorMsg(1)
      }
    })
  }
  //公用错误提示
  _errorMsg(msg){
    if (!msg) msg = 1
    let tips = tip[msg] ? tip[msg] : tip[1]
    wx.showToast({
      title: tips,
      icon: 'none',
      duration: 2000
    })
  }
}

export {
  HTTP
}