import { HTTP } from "../utils/http-promise.js"

class SearchModel extends HTTP {
  key = 'history'
  maxLength = 10
  getHistoryList(){
    return wx.getStorageSync(this.key) || []
  }

  getHotList(){
    return this.request({
      url: '/book/hot_keyword'
    })
  }

  setHistory(keyword){
    let historyVal = this.getHistoryList()
    const isInArr = historyVal.includes(keyword)
    if(!isInArr){
      if(historyVal.length>=10){
        historyVal.pop()
      }
      historyVal.unshift(keyword)
      wx.setStorageSync(this.key, historyVal)
    }
  }
}

export {
  SearchModel
}