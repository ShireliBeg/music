import { HTTP } from '../utils/http.js'

class LikeModel extends HTTP {
  getLike(isLike, id, type){
    let url = isLike === 'cancel' ? '/like/cancel' : '/like'
    console.log(url)
    this.request({
      url: url,
      method: 'POST',
      data:{
        art_id: id,
        type: type
      }
    })
  }

  getClassicLikeStatus(cid, type, success) {
    var params = {
      url: '/classic/' + type + '/' + cid + '/favor',
      success: success
    }
    this.request(params)
  }
}

export {
  LikeModel
}