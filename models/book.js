import { HTTP } from "../utils/http-promise.js"

class BookModel extends HTTP{
  getHotList(){
    return this.request({ url: '/book/hot_list' })
  }
  // 搜索关键字
  getSearch(keyword, start){
    return this.request({
      url: '/book/search',
      data: {
        q: keyword,
        start: start,
        summary: 1
      }
    })
  }

  getMyBookCount(){
    return this.request({url: '/book/favor/count'})
  }

  getDetail(bid) {
    return this.request({ url: `/book/${bid}/detail` })
  }

  getLikeStatus(bid) {
    return this.request({ url: `/book/${bid}/favor` })
  }

  getComment(bid) {
    return this.request({ url: `/book/${bid}/short_comment` })
  }

  postComment(bid,comment){
    return this.request({
      url: '/book/add/short_comment',
      method: "POST",
      data: {
        book_id: bid,
        content: comment
      }
    })
  }
}

export {
  BookModel
}